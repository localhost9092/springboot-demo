package cn.king.controller;

import cn.king.mapper.UserMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserMapper userMapper;
    
    // http://localhost:8888/user
    @GetMapping
    public Object get() {
        return userMapper.selectList(Wrappers.emptyWrapper());
    }
    
}
