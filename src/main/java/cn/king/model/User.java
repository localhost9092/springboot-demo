package cn.king.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@TableName(value ="t_user")
@Data
public class User {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String name;

    private Date birthday;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    
}
