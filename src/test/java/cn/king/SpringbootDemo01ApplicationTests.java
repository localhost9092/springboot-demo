package cn.king;

import cn.king.mapper.UserMapper;
import cn.king.model.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Date;

@Slf4j
@SpringBootTest
class SpringbootDemo01ApplicationTests {

    @Resource
    private DataSource dataSource;
    @Resource
    private UserMapper userMapper;

    @Test
    public void test01() throws Exception {
        log.info(dataSource.getConnection().toString());
    }

    @Test
    public void test02() throws Exception {
        Date now = new Date();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setName("username" + i);
            user.setBirthday(now);
            userMapper.insert(user);
        }
    }

}
