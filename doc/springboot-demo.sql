CREATE DATABASE `springboot-demo`;
USE `springboot-demo`;

DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `birthday` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

INSERT INTO `t_user` VALUES (1, 'username0', '2023-05-20 15:02:49');
INSERT INTO `t_user` VALUES (2, 'username1', '2023-05-20 15:02:49');
INSERT INTO `t_user` VALUES (3, 'username2', '2023-05-20 15:02:49');
INSERT INTO `t_user` VALUES (4, 'username3', '2023-05-20 15:02:49');
INSERT INTO `t_user` VALUES (5, 'username4', '2023-05-20 15:02:49');
INSERT INTO `t_user` VALUES (6, 'username5', '2023-05-20 15:02:49');
INSERT INTO `t_user` VALUES (7, 'username6', '2023-05-20 15:02:49');
INSERT INTO `t_user` VALUES (8, 'username7', '2023-05-20 15:02:49');
INSERT INTO `t_user` VALUES (9, 'username8', '2023-05-20 15:02:49');
INSERT INTO `t_user` VALUES (10, 'username9', '2023-05-20 15:02:49');

